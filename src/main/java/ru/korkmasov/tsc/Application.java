package ru.korkmasov.tsc;


import ru.korkmasov.tsc.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    private static void displayHelp() {
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
        System.out.println("exit - Close Application.");
    }

    private static void displayVersion() {
        System.out.println("1.0.0");
    }

    private static void displayAbout() {
        System.out.println("Djalal Korkmasov");
        System.out.println("dkorkmasov@tsc.com");
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static boolean run(final String[] args) {
        if (args == null || args.length == 0) return false;
        if (args.length < 1) return false;
        final String param = args[0];
        run(param);
        return true;
    }

    private static void run(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case TerminalConst.CMD_HELP:
                displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                displayAbout();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
                break;
            default:
                showIncrorrectCommand();
        }
    }

    private static void showIncrorrectCommand() {
        System.out.println("Error! Comand not found!");
    }

    private static void exit() {
        System.exit(0);
    }

    public static void main(final String[] args) {
        displayWelcome();
        if (run(args)) System.exit(0);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("*ENTER COMMAND*");
            final String command = scanner.nextLine();
            run(command);
        }
    }
}
